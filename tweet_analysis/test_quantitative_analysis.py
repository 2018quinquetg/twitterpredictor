import pytest
from tweet_analysis.quantitative_analysis import *
from twitter_collect.collect import collect, transform_to_dataframe

#Collecting and transforming the data on which to test
data = transform_to_dataframe(collect())

def test_max_retweets(data):
    assert type(extract_most_retweeted(data)[0]) == str
    assert type(extract_most_retweeted(data)[1]) == int

def test_max_likes(data):
    assert type(extract_most_liked(data)[0]) == str
    assert type(extract_most_liked(data)[1]) == int

def test_average_retweet(data):
    assert type(extract_average_retweet(data)) == int

def test_average_likes(data):
    assert type(extract_average_likes(data)) == int
     

