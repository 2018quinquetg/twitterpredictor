from textblob import *
from textblob_fr import PatternAnalyzer, PatternTagger
from twitter_collect.collect import *

data = transform_to_dataframe(collect(1))

text_blobs = []

for text in data['text']:
    unparsed_text = TextBlob(text, pos_tagger=PatternTagger(), analyzer=PatternAnalyzer()).words
    for word, tag in unparsed_text.tags:
        if tag != 'DT':
            text_blobs.append(word)

text_blobs = list(set(text_blobs))


print(text_blobs)
