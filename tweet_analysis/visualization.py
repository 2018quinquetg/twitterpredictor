import pandas as pd
import matplotlib.pyplot as plt
from tweet_analysis import quantitative_analysis

def show_retweets_vs_likes(data):
    tfav = pd.Series(data=data['likes'].values, index=data['date'])
    tret = pd.Series(data=data['retweets'].values, index=data['date'])

    # Likes vs retweets visualization:
    tfav.plot(figsize=(16, 4), label="Likes", legend=True)
    tret.plot(figsize=(16, 4), label="Retweets", legend=True)

    plt.show()

def show_retweets_vs_average(data):
    tret = pd.Series(data=data['retweets'].values, index=data['date'])
    avg_ret = pd.Series(data=quantitative_analysis.extract_average_retweet(data), index=data['date'])

    # Likes vs retweets visualization:
    tret.plot(figsize=(16, 4), label="Retweets", legend=True)
    avg_ret.plot(figsize=(16, 4), label="Average Retweets", legend=True)

    plt.show()

def show_likes_vs_average(data):
    tlike = pd.Series(data=data['likes'].values, index=data['date'])
    avg_like = pd.Series(data=quantitative_analysis.extract_average_retweet(data), index=data['date'])

    # Likes vs retweets visualization:
    tlike.plot(figsize=(16, 4), label="Likes", legend=True)
    avg_like.plot(figsize=(16, 4), label="Average Likes", legend=True)

    plt.show()

