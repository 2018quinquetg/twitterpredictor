import numpy as np
from twitter_collect.collect import *

def extract_most_retweeted(dataframe):
    """Takes the data as a dataframe and returns the text and the number of retweets of the most retweeted tweet in data"""

    max_retweets = np.max(dataframe["retweets"])
    index = dataframe[dataframe["retweets"] == max_retweets].index[0]

    return dataframe['text'][index], int(max_retweets)

def extract_most_liked(dataframe):
    """Takes the data as a dataframe and returns the text and the number of likes of the most liked tweet in data"""

    max_likes = np.max(dataframe["likes"])
    index = dataframe[dataframe["likes"] == max_likes].index[0]

    return dataframe['text'][index], int(max_likes)


def extract_average_retweet(dataframe):
    """Takes the data as a dataframe and returns the average number of retweets"""

    total_retweet = np.sum(dataframe["retweets"])
    length = len(dataframe["retweets"])

    return int(total_retweet/length)


def extract_average_likes(dataframe):
    """Takes the data as a dataframe and returns the average number of likes"""

    total_likes = np.sum(dataframe["likes"])
    length = len(dataframe["likes"])

    return int(total_likes/length)
