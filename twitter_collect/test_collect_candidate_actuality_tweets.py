from twitter_collect.collect_candidate_actuality_tweets import get_tweets_from_candidates_search_queries
from twitter_collect.twitter_connection_setup import twitter_setup

def test_get_tweets_from_candidates_search_queries():
    connexion = twitter_setup()
    queries = ['Emmanuel Macron']
    tweets = get_tweets_from_candidates_search_queries(queries, connexion)

    assert len(tweets) > 0
