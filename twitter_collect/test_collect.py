from twitter_collect import collect
import pandas

candidateNums = [1, 2]

#Testing the creation of a valid dataframe by the collect function
def test_collect():
    for candidateNum in candidateNums:
        tweets = collect.collect(candidateNum)
        assert isinstance(tweets, list)
        assert isinstance(tweets[0], object)

def test_transform_to_dataframe():

    for candidateNum in candidateNums:
        tweets = collect.collect(candidateNum)
        data = collect.transform_to_dataframe(tweets)
        assert 'tweet_id' in data.columns
        assert 'date' in data.columns
        assert 'author' in data.columns
        assert 'text' in data.columns
        assert 'hashtags' in data.columns
        assert 'retweets' in data.columns
        assert 'likes' in data.columns
        assert isinstance(data, pandas.DataFrame)

