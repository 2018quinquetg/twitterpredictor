from twitter_collect.collect_candidate_queries import *
from twitter_collect.collect_tweet_candidate_activity import *
from twitter_collect.collect_candidate_actuality_tweets import *
from twitter_collect.tweet_collect_whole import *
from twitter_collect.tweet_storage import store_tweets

#Global candidate list to simplify the work. In real conditions, should be extracted from a text
# file like the screen names.
candidate_list = ['EmmmanuelMacron', 'EdouardPhilippePM']
candidate_queries = {}
tweets_queries = {}
candidate_replies = {}
candidate_retweets = {}
connexion = twitter_setup()


# Fetching the queries and then getting the different tweet required from the twitter API, for each candidate in the candidate_list. This candidate list should be imported from CandidateData
for i, candidate in enumerate(candidate_list):

    #Fetching the queries for each candidae and storing them. Ordered by candidate number.
    candidate_queries['queries_candidate_{0}'.format(i+1)] = get_candidate_queries(i+1, 'keywords_candidate')+get_candidate_queries(i+1, 'hashtags_candidate')

    #Fetching and storing the tweets corresponding to the queries for each candidate in a "tweets_queries" dictionary.
    tweets_queries['tweets_queries_candidate_{0}'.format(i+1)] = get_tweets_from_candidates_search_queries(candidate_queries['queries_candidate_{0}'.format(i+1)], connexion)

    #Fetching and storing in a dictionary the replies to the tweets of the candidates. Ordered by candidate number.
    candidate_replies['replies_to_candidate_{0}'.format(i+1)] = get_replies_to_candidate(i+1, connexion)

    #Fetching and storing in a dictionary the retweets ot the candidates tweets. Ordered by candidate number.
    candidate_retweets['retweets_to_candidate_{0}'.format(i+1)] = get_retweets_of_candidate(i+1, connexion)


#Storing the tweets in files depending on their type
for i, candidate in enumerate(candidate_list):
    store_tweets(tweets_queries['tweets_queries_candidate_{0}'.format(i+1)],'tweets_queries_candidate_{0}'.format(i+1))
    store_tweets(candidate_replies['replies_to_candidate_{0}'.format(i+1)],'replies_to_candidate_{0}'.format(i+1))
    store_tweets(candidate_retweets['retweets_to_candidate_{0}'.format(i+1)],'retweets_to_candidate_{0}'.format(i+1))
