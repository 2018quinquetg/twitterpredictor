import json

def store_tweets(tweets, filename):
    tweets_to_store = {}

    #Selecting the main data from each tweet in the tweets and storing it in the json format in a dictionary
    for i, tweet in enumerate(tweets):
            tweets_to_store['tweet_{0}'.format(i)] = {
                "tweet_id": tweet['id'],
                "author": tweet['user']['id'],
                "text": tweet['text'],
                "date": tweet['created_at'],
                "hashtags": tweet['entities']['hashtags'],
                "number_of_retweets": tweet['retweet_count'],
                "number_of_likes": tweet['user']['favourites_count']
            }

    #Storing the tweets in a file. Creating the file if it does not exist.
    with open('../CandidateData/'+filename+'.json', 'w+', encoding='utf-8') as file:
            json.dump(tweets_to_store, file, indent=4)
