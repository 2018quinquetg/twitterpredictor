from twitter_collect.twitter_connection_setup import *

api = twitter_setup()

#Checks that the api object is of the class API
def test_twitter_setup():
    assert isinstance(api, object)

#Checks that the api connexion receives tweets
def test_json():
    assert len(api.home_timeline()) != 0

