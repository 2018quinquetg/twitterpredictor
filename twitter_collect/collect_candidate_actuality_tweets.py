import tweepy

def get_tweets_from_candidates_search_queries(queries, twitter_api):
    '''search and return all tweets responding to a list of queries'''
    collection = []
    try:
        for query in queries:
            tweets = twitter_api.search(q=query, lang="fr", rpp=10)
            for tweet in tweets:
                collection.append(tweet._json)
        return collection
    except tweepy.RateLimitError:
        print("hitting Twitters rate limit")
    except tweepy.TweepError as e:
        print("TweepError: "+e.response.text)

