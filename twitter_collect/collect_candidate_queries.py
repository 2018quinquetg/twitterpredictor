def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    try:
        with open("../CandidateData/"+str(file_path)+'_'+str(num_candidate)+'.txt', 'r', encoding='utf-8') as file:
            query_list=[]

            for line in file:
                query_list.append(line[:-1])
        return query_list

    except IOError:
        print('Unable to read file: %s_%s' % (file_path, num_candidate))

