from tweepy.error import *
from tweepy.streaming import StreamListener
from twitter_collect.twitter_connection_setup import *


class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if  str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True

def collect_by_streaming(screen_names):

    try:
        connexion = twitter_setup()
        listener = StdOutListener()
        stream=tweepy.Stream(auth = connexion.auth, listener=listener)
        for query in screen_names:
            stream.filter(track=[query])
            stream.filter(track=['to:'+query])
            stream.filter(track=['from:'+query])

    except RateLimitError:
        print("The number of requests has been exceeded. Please wait for 15mn before trying again")
    except TweepError as e:
        print("TweepError: "+e.response.text)

