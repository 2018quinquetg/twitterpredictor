import json
import pandas as pd

#Module collecting the data stored in text files and preparing it for analysis by ordering it in a dataframe

def collect(candidateNum):
    """Extracts data previously stored in the CandidateData directory in order for it to be analyzed"""
    tweets = []

    # Extracting tweets from the storage files. The main needs to be ran in order for these files to exist.

    #Fetching the tweets corresponding to the queries for the candidate
    with open('../CandidateData/tweets_queries_candidate_{0}.json'.format(candidateNum), 'r') as file:
        tweets += json.load(file).values()

    #Fetching the tweets corresponding to the replies to the candidate
    with open('../CandidateData/replies_to_candidate_{0}.json'.format(candidateNum), 'r') as file:
        tweets += json.load(file).values()

    #Fetching the tweets corresponding to the retweets of the candidate
    with open('../CandidateData/retweets_to_candidate_{0}.json'.format(candidateNum), 'r') as file:
        tweets += json.load(file).values()

    return tweets

def transform_to_dataframe(tweets):
    """Takes json-formated tweets containing the id, date, author, text and hashtags of the tweets as an input and
    returns a pandas DataFrame object containing the data"""

    ids = []
    dates = []
    authors = []
    texts = []
    hashtags = []
    retweet_count = []
    likes = []

    # Extracting each type of data from the tweets into lists
    for tweet in tweets:
        ids.append(tweet["tweet_id"])
        dates.append(tweet["date"])
        authors.append(tweet["author"])
        texts.append(tweet["text"])
        hashtags.append(tweet["hashtags"])
        retweet_count.append(tweet["number_of_retweets"])
        likes.append(tweet["number_of_likes"])

    # Making an indexed DataFrame out of the Series
    dataframe = pd.DataFrame({
        "tweet_id": pd.Series(ids),
        "date": pd.Series(dates),
        "author": pd.Series(authors),
        "text": pd.Series(texts),
        "hashtags": pd.Series(hashtags),
        "retweets": pd.Series(retweet_count),
        "likes": pd.Series(likes)
    })

    return dataframe
