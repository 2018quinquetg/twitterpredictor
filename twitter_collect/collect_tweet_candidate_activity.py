from twitter_collect.twitter_connection_setup import *
from tweepy.error import *


#Gets the candidates user ids from their screen name and store them in a list
def get_candidate_id(twitter_api):
    screen_name_list = []
    candidate_list = []

    try:
        with open("../CandidateData/screen_names.txt", 'r', encoding='utf-8') as file:
            for line in file:
                screen_name_list.append(line[:-1])

        for screen_name in screen_name_list:
            candidate_list.append(twitter_api.get_user(screen_name).id)
        return candidate_list, screen_name_list

    except IOError:
        print('Unable to read file: "../CandidateData/screen_names.txt')
    except RateLimitError:
        print("The number of requests has been exceeded. Please wait for 15mn before trying again")
    except TweepError as e:
        print("TweepError: "+e.response.text)


#Gets the ids of the tweets posted by the candidate, and stores them in a dictionnary for later analysis.
def get_candidate_tweets_ids(num_candidate, twitter_api):
    try:
        candidate_list, screen_name_list = get_candidate_id(twitter_api)
        tweets_by_candidate_ids = []
        tweets = twitter_api.user_timeline(user_id=candidate_list[num_candidate-1], count=5)

        for tweet in tweets:
            tweets_by_candidate_ids.append(tweet._json['id'])

        return screen_name_list, tweets_by_candidate_ids

    except RateLimitError:
        print("The number of requests has been exceeded. Please wait for 15mn before trying again")
    except TweepError as e:
        print("TweepError: "+e.response.text)


def get_replies_to_candidate(num_candidate, twitter_api):

    try:
        screen_name_list, tweets_by_candidate_ids = get_candidate_tweets_ids(num_candidate, twitter_api)
        replies = []

        for i, tweet_id in enumerate(tweets_by_candidate_ids):
            tweets_to_candidate = twitter_api.search('to: '+screen_name_list[num_candidate-1], lang="fr", since_id=tweet_id, rpp = 100)

            for tweet in tweets_to_candidate:
                if "in_reply_to_status_id" in tweet._json:
                    if tweet._json["in_reply_to_status_id"] == tweet_id:
                        replies.append(tweet._json)
        return replies

    except RateLimitError:
        print("The number of requests has been exceeded. Please wait for 15mn before trying again")
    except TweepError as e:
        print("TweepError: "+e.response.text)


#Gets the retweets of the candidate tweets, based on the tweet dictionary
def get_retweets_of_candidate(num_candidate, twitter_api):
    '''search and return all retweets of a tweet of a candidate, and the information of that tweet'''

    try:
        screen_name_list, tweets_by_candidate_ids = get_candidate_tweets_ids(num_candidate, twitter_api)
        retweets = []
        for tweet_id in tweets_by_candidate_ids:
            retweets_int = twitter_api.retweets(tweet_id, count=2)
            if len(retweets_int)>0:
                retweets.append(retweets_int[0]._json)

        return retweets

    except RateLimitError:
        print("The number of requests has been exceeded. Please wait for 15mn before trying again")
    except TweepError as e:
        print("TweepError: "+e.response.text)
